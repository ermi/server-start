#!/usr/bin/python3

#  server-start: Start server with wake-on-lan, wait for it
#                and enter crypto password over ssh
#  Copyright (C) 2024  ermi
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pyping
import os
import sys
import subprocess
import datetime
import time
import json


class ping_test(object):
	def __init__(self, host, tmax, interval):
		base = pyping.icmp()
		base.connect(host)
		self.obj = pyping.async_sender(base)
		self.obj.start(interval)
		
		self.start_ts = time.monotonic()
		self.end_ts = self.start_ts+tmax
		self.ts = self.start_ts
		try:
			while self.poll(2*interval)==None:
				pass
		except TimeoutError:
			self.unreachable = True
		except KeyboardInterrupt:
			self.unreachable = True
		else:
			self.unreachable = False
		finally:
			self.obj.stop()
	
	def __del__(self):
			self.obj.join()
	
	def poll(self, interval):
		if self.ts>=self.end_ts:
			raise TimeoutError
		self.ts += interval
		ts = min(self.ts, self.end_ts)
		
		try:
			icmp_seq, dt, clock = self.obj.recv(ts)
		except BlockingIOError:
			print('{0:.0f} s'.format(self.dt()))
		else:
			dt = self.dt()
			print('Host up after {0:.1f} s'.format(dt))
			return dt
	
	def dt(self):
		return time.monotonic()-self.start_ts


def start(cnf):
	try:
		wol_cmd = cnf['wol']
	except KeyError:
		pass
	else:
		print(wol_cmd)
		pwol = subprocess.run(wol_cmd, stdout=subprocess.DEVNULL)
	
	try:
		host = cnf['host']
	except KeyError:
		pass
	else:
		pinger = ping_test(host, cnf.get('timeout', 60), cnf.get('interval', 1))
		if pinger.unreachable:
			print('server unreachable')
			return
	
	try:
		time.sleep(cnf['delay'])
	except KeyError:
		pass
	
	try:
		unlock_cmd = cnf['unlock']
	except KeyError:
		pass
	else:
		print(unlock_cmd)
		subprocess.run(unlock_cmd)


def readconf():
	try:
		with open(os.path.expanduser('~/.server-start.json')) as f:
			return json.load(f)
	except FileNotFoundError:
		with open('/etc/server-start.json') as f:
			return json.load(f)

if __name__=='__main__':
	cnf = readconf()
	if len(sys.argv)>1:
		for name in sys.argv[1:]:
			try:
				data = cnf[name]
			except KeyError:
				print('Error: name "', name, '" is not known', sep='', file=sys.stderr)
				if locals().get('first_error', True):
					print('Known names are:', list(cnf.keys()), file=sys.stderr)
					first_error = False
			else:
				start(data)
	else:
		start(list(cnf.values())[0])
